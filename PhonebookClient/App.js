const settings = require('./settings');
import React from 'react';
import Axios from 'axios';
import Details from './components/Details';
import Tags from './components/Tags';
import ColorPicker from './components/ColorPicker'
import { RefreshControl, StyleSheet, Text, View, Button, TextInput, Picker } from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";
import { Provider, observer } from 'mobx-react';
import ContactsStore from './stores/contactsStore'
import { ScrollView } from 'react-native-gesture-handler';

const data = ContactsStore.create({ contacts: [], tags: [], tagId: 0, filter: "", refreshing: false});
var timeout = null;

@observer
class App extends React.Component {
  componentDidMount = () => {
    this.getData();
  }

  willFocusSubscription = this.props.navigation.addListener(
    'willFocus',
    payload => {
      this.getData();
    }
  );

  searchInput = (text) => {
    clearTimeout(timeout);

    timeout = setTimeout(() => {
        data.setFilter(text);
        this.getData();
    }, 500);
  }

  getData = () => {
    Axios.get(`${settings.apiUrl}/contacts/?filter=${data.filter}&tagId=${data.tagId}`)
    .then((response) => {
      data.setContacts(response.data.contacts);
      data.setTags(response.data.tags);
    }).catch((error) => {
      console.log(error.data);
    });
  }

  getTagStyle = (color) => {
    return {
      backgroundColor: color,
      height: 30,
      paddingLeft: 10,
      paddingRight: 10,
      borderRadius: 15,
      justifyContent: 'center',
      marginRight: 5,
      marginTop: 2,
      marginBottom: 2
    }
  }

  deleteContact = (id) => {
    Axios.delete(settings.apiUrl + '/contacts/' + id)
    .then((response) => {
      this.getData();
    }).catch((error) => {
      console.log(error.data);
    });
  }

  _onRefresh = () => {
    data.toggleRefreshing();
    this.getData();
    data.toggleRefreshing();;
  }

  render() {
    return (
      <Provider ContactsStore={ContactsStore}>
        <View style={styles.container}>
          <View style={styles.searchBar}>
            <Text>Search:</Text>
            <TextInput
              style={styles.search}
              onChangeText={(text) => {this.searchInput(text)}}
            />
          </View>
          <View style={styles.tagView}>
            <Text>Tags:</Text>
            <View style={{borderWidth: 1, marginLeft: 5}}>
              <Picker
                selectedValue={data.tagId}
                onValueChange={(itemValue) => {
                  data.setTagFilter(itemValue);
                  this.getData();
                }}
                style={{height: 30, width: 150}}>
                <Picker.Item label="None" value={0}/>
                {data.tags.map(x =>
                  <Picker.Item key={x.id} label={x.title} value={x.id}/>
                )}
              </Picker>
            </View>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
              <Button
                title="Edit tags"
                onPress={() => this.props.navigation.navigate('Tags', {id: 0})}
              />
            </View>
          </View>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={data.refreshing}
                onRefresh={this._onRefresh}
              />}>
            {data.contacts.map( contact =>
              <View key={contact.id} style={styles.contactRow}>
                <View style={{flexDirection: 'column', flex: 1}}>
                  <Text style={styles.name}>{contact.getFullName()}</Text>
                  <View style={styles.tags}>
                    {contact.tags.map((tag, index) => <View key={index} style={this.getTagStyle(tag.color)}><Text>{tag.title}</Text></View>)}
                  </View>
                </View>
                <View style={styles.editButton}>
                  <Button
                    title="Edit"
                    onPress={() => this.props.navigation.navigate('Details', {id: contact.id})}
                  />
                  <Button
                    title="Delete"
                    color="red"
                    onPress={() => this.deleteContact(contact.id)}
                  />
                </View>
              </View>
            )}
          </ScrollView>
          <View style={{borderTopWidth: 1}}>
            <Button
              title="New Contact"
              onPress={() => this.props.navigation.navigate('Details', {id: 0})}
            />
          </View>
        </View>
      </Provider>
    );
  }
}

const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: App
    },
    Details: Details,
    Tags: Tags,
    ColorPicker: ColorPicker
  },
  {
    initialRouteName: "Home"
  }
);

export default createAppContainer(AppNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffff'
  },
  searchBar: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    borderWidth: 1,
    height: 50,
    alignItems: 'center'
  },
  search: {
    borderWidth: 1,
    flex: 1,
    height: 40,
    marginLeft: 10,
    marginRight: 5
  },
  tags: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap'
  },
  contactRow: {
    flexDirection:'row',
    borderBottomWidth: 1
  },
  name: {
    fontSize: 25,
    paddingLeft: 5,
    paddingTop: 5,
    flex: 3
  },
  editButton: {
    flexDirection: 'row'
  },
  tagView: {
    height: 50,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 5,
    paddingRight: 5
  }
});
