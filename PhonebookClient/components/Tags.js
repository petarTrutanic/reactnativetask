const settings = require('../settings');
import React from 'react';
import Axios from 'axios';
import { RefreshControl, TouchableOpacity, StyleSheet, Text, TextInput, Button, View } from 'react-native';
import { Provider, observer } from 'mobx-react';
import { TagStore } from '../stores/contactsStore';
import { ScrollView } from 'react-native-gesture-handler';

var data = TagStore.create({ tags: [], refreshing: false });

@observer
export default class Tags extends React.Component {
  componentDidMount = () => {
    this.getData();
  }

  handleColorChange = (color, index) => {
    data.setTagColor(color, index);
  }

  getData = () => {
    Axios.get(settings.apiUrl + `/tags`)
    .then((response) => {
        data.setTags(response.data);
    }).catch((error) => {
      console.log(error);
    });
  }

  saveChanges = () => {
    Axios.post(`${settings.apiUrl}/tags`, data.tags)
    .then((response) => {
      this.props.navigation.navigate('Home')
    }).catch((error) => {
      console.log(error);
    });
  }

  tagColor = (color) => {
    return {
        width: 40,
        height: 40,
        borderWidth: 1,
        backgroundColor: color
      }
  }

  _onRefresh = () => {
    data.toggleRefreshing();
    this.getData();
    data.toggleRefreshing();;
  }

  render() {
    return (
      <Provider TagStore={TagStore}>
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={{fontSize: 25}}>Tags:</Text>
                <Button
                    title="New"
                    onPress={() => data.addNewTag()}
                    color='green'
                />
            </View>
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={data.refreshing}
                  onRefresh={this._onRefresh}
                />}>
                {data.tags.map((x, index) =>
                    <View key={index} style={styles.tagRow}>
                        <TextInput
                            style={styles.tagName}
                            onChangeText={(value) => data.setTagTitle(value, index)}
                            value={x.title}
                        />
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('ColorPicker', {color: x.color, tagIndex: index, setColor: this.handleColorChange })}
                            style={this.tagColor(x.color)}>
                        </TouchableOpacity>
                        <Button
                            title="Delete"
                            onPress={() => data.deleteTag(index)}
                            color='red'
                        />
                    </View>
                )}
            </ScrollView>
            <View style={{borderTopWidth: 1}}>
            <Button
              title="Save"
              onPress={() => this.saveChanges()}
            />
          </View>
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  tagRow: {
      flexDirection: 'row',
      height: 60,
      alignItems: 'center',
      justifyContent: 'space-between',
      marginLeft: 5,
      marginRight: 5
  },
  header: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      margin: 5
  },
  tagName: {
      width: '40%',
      borderBottomWidth: 1
  }
});
