const settings = require('../settings');
import React from 'react';
import { Dimensions, StyleSheet, Button, View } from 'react-native';
import { ColorWheel } from 'react-native-color-wheel';
import ColorStore from '../stores/colorStore';

const data = ColorStore.create({color: "#ffffff", tagIndex: -1})

export default class ColorPicker extends React.Component {
  componentWillMount = () => {
    var params = this.props.navigation.state.params;

    data.setTagIndex(params.tagIndex);
    data.setColor(params.color);
  }

  saveChanges = () => {
    this.props.navigation.state.params.setColor(data.color, data.tagIndex);
    this.props.navigation.goBack();
  }

  render() {
    return (
        <View style={styles.container}>
            <ColorWheel
                initialColor={data.color}
                onColorChangeComplete={color => data.setColorHsv(color)}
                style={{width: Dimensions.get('window').width}}
                thumbStyle={{ height: 30, width: 30, borderRadius: 30}}
            />
            <View style={{borderTopWidth: 1}}>
                <Button
                title="Confirm"
                onPress={() => this.saveChanges()}
                />
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  }
});
