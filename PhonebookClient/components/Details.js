const settings = require('../settings');
import React from 'react';
import Axios from 'axios';
import { StyleSheet, Text, TextInput, Button, View } from 'react-native';
import { Contact, TagStore } from '../stores/contactsStore';
import { CheckBox } from 'react-native-elements'
import { Provider, observer } from 'mobx-react';
import { ScrollView } from 'react-native-gesture-handler';

const contact = Contact.create({
  id: 0,
  firstName : "",
  lastName : "",
  notes : "",
  address : "",
  emailAddresses: [],
  phoneNumbers: [],
  tags: []
 })

 var data = TagStore.create({ tags: []})

@observer
export default class App extends React.Component {
  componentDidMount = () => {
    var contactId = this.props.navigation.state.params.id;
    if(contactId === 0){
      contact.reset();
    }

    Axios.get(settings.apiUrl + `/contacts/${contactId}`)
    .then((response) => {
      if(response.data.contact !== null)
        contact.setContact(response.data.contact);
      data.setTags(response.data.tags);
    }).catch((error) => {
      console.log(error);
    });
  }

  saveChanges = () => {
    Axios.post(settings.apiUrl + `/contacts`, contact)
    .then((response) => {
      this.props.navigation.navigate('Home')
    }).catch((error) => {
      console.log(error);
    });
  }

  render() {
    return (
      <Provider ContactsStore={ContactsStore}>
        <View style={styles.container}>
          <ScrollView>
            <View style={styles.row}>
              <Text style={styles.text}>First Name: </Text>
              <TextInput
                style={styles.input}
                onChangeText={(text) => contact.setFirstName(text)}
                value={contact.firstName}
              />
            </View>
            <View style={styles.row}>
              <Text style={styles.text}>Last Name: </Text>
              <TextInput
                style={styles.input}
                onChangeText={(text) => contact.setLastName(text)}
                value={contact.lastName}
              />
            </View>
            <View style={styles.row}>
              <Text style={styles.text}>Address: </Text>
              <TextInput
                style={styles.input}
                onChangeText={(text) => contact.setAddress(text)}
                value={contact.address}
              />
            </View>
            <View style={styles.row}>
              <Text style={styles.text}>Notes: </Text>
              <TextInput
                style={styles.input}
                onChangeText={(text) => contact.setNotes(text)}
                value={contact.notes}
              />
            </View>
            <View style={styles.listContainer}>
              <View style={styles.flexRow}>
                <Text style={styles.listTitle}>Numbers</Text>
                <View style={styles.addButton}>
                  <Button
                    title="New number"
                    color="green"
                    onPress={() => contact.addNewNumber()}
                  />
                </View>
              </View>
              <View>
                {contact.phoneNumbers.map((phoneNumber, index) =>
                  <View key={index} style={styles.listItem}>
                    <TextInput
                      style={styles.input}
                      onChangeText={(value) => contact.setNumber(value, index)}
                      value={phoneNumber.number}
                    />
                    <Button
                    title="Delete"
                    onPress={() => contact.deleteNumber(index)}
                    color='#ff0000'
                    />
                  </View>
                )}
              </View>
            </View>
            <View style={styles.listContainer}>
              <View style={styles.flexRow}>
                <Text style={styles.listTitle}>Emails</Text>
                <View style={styles.addButton}>
                  <Button
                    title="New email"
                    color="green"
                    onPress={() => contact.addNewEmailAddress()}
                  />
                </View>
              </View>
              <View>
                {contact.emailAddresses.map((emailAddresses, index) =>
                  <View key={index} style={styles.listItem}>
                    <TextInput
                      style={styles.input}
                      onChangeText={(value) => contact.setEmailAddress(value, index)}
                      value={emailAddresses.email}
                    />
                    <Button
                    title="Delete"
                    onPress={() => contact.deleteEmailAddress(index)}
                    color='#ff0000'
                    />
                  </View>
                )}
              </View>
            </View>
            <View style={styles.listContainer}>
              <Text style={styles.listTitle}>Tags:</Text>
              {data.tags.map(tag => 
                <CheckBox
                  key={tag.id}
                  containerStyle={{backgroundColor: tag.color}}
                  title={tag.title}
                  checked={contact.hasTag(tag.id)}
                  onPress={() => {contact.toggleTag(tag)}}
                />
              )}
            </View>
          </ScrollView>
          <View style={{borderTopWidth: 1}}>
            <Button
              title="Save"
              onPress={() => this.saveChanges()}
            />
          </View>
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  row: {
    flexDirection: 'row',
    height: 50,
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
  },
  input : {
    fontSize: 25,
    flex: 1,
    borderBottomWidth: 1,
    marginRight: 5
  },
  text: {
    fontSize: 25
  },
  listContainer: {
    paddingLeft: 10,
    paddingRight: 10,
    borderTopWidth: 1,
    marginTop: 10
  },
  addButton: {
    marginTop: 5
  },
  flexRow: {
    flexDirection: 'row'
  },
  listTitle: {
    flex: 1,
    fontSize: 25
  },
  listItem: {
    flexDirection: 'row',
    marginTop: 5
  }
});
