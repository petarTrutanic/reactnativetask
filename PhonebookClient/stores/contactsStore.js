import { types } from "mobx-state-tree";
import _ from 'underscore';

const Tag = types.model({
    id: types.integer,
    title: types.string,
    color: types.string
})

export const TagStore = types.model("TagsStore", {
  tags: types.array(Tag),
  refreshing: types.optional(types.boolean, false)
}).actions(self => ({
    setTags(tags) {
      self.tags = tags;
    },
    addNewTag() {
      self.tags.push({
        id: 0,
        title: '',
        color: '#ffffff'
      })
    },
    setTagTitle(title, index) {
      console.log(index);
      console.log(self.tags);
      self.tags[index].title = title;
    },
    setTagColor(color, index) {
      self.tags[index].color = color;
    },
    deleteTag(index) {
      self.tags.splice(index, 1);
    },
    toggleRefreshing() {
      self.refreshing = !self.refreshing;
    }
}))

const ListContact = types.model({
  id: types.integer,
  firstName: types.string,
  lastName: types.string,
  tags: types.array(Tag)
}).actions(self => ({
  getFullName() {
    return `${self.firstName} ${self.lastName}`;
  }
}))
  
export default ContactsStore = types.model("ContactsStore", {
  contacts: types.array(ListContact),
  tags: types.array(Tag),
  tagId: types.integer,
  filter: types.string,
  refreshing: types.boolean
}).actions(self => ({
    setContacts(contacts) {
      self.contacts = contacts;
    },
    setTagFilter(tagId) {
      self.tagId = tagId;
    },
    setTags(tags) {
      self.tags = tags;
    },
    setFilter(value) {
      self.filter = value;
    },
    toggleRefreshing() {
      self.refreshing = !self.refreshing;
    }
}))

const PhoneNumber = types.model({
  id: types.integer,
  number: types.string,
  contactId: types.integer
})

const EmailAddress = types.model({
  id: types.integer,
  email: types.string,
  contactId: types.integer
})

export const Contact = types.model({
  id: types.integer,
  firstName: types.string,
  lastName: types.string,
  address: types.string,
  notes: types.string,
  tags: types.array(Tag),
  phoneNumbers: types.array(PhoneNumber),
  emailAddresses: types.array(EmailAddress)
}).actions(self => ({
  getFullName() {
    return `${self.firstName} ${self.lastName}`;
  },
  setContact(contact) {
    self.firstName = contact.firstName;
    self.lastName = contact.lastName;
    self.id = contact.id;
    self.address = contact.address;
    self.notes = contact.notes;
    self.phoneNumbers = contact.phoneNumbers;
    self.tags = contact.tags;
    self.emailAddresses = contact.emailAddresses;
  },
  setFirstName(firstName) {
    self.firstName = firstName;
  },
  setLastName(lastName) {
    self.lastName = lastName;
  },
  setAddress(address) {
    self.address = address;
  },
  setNotes(notes) {
    self.notes = notes;
  },
  addNewNumber() {
    self.phoneNumbers.push({
      id: 0,
      number: '',
      contactId: self.id,
      isDeleted: false
    })
  },
  setNumber(number, index) {
    self.phoneNumbers[index].number = number;
  },
  deleteNumber(index) {
    self.phoneNumbers.splice(index, 1);
  },
  addNewEmailAddress() {
    self.emailAddresses.push({
      id: 0,
      email: '',
      contactId: self.id,
      isDeleted: false
    })
  },
  setEmailAddress(emailAddresses, index) {
    self.emailAddresses[index].email = emailAddresses;
  },
  deleteEmailAddress(index) {
    self.emailAddresses.splice(index, 1);
  },
  deleteTag(id) {
    self.tags = self.tags.filter(x => x.id !== id);
  },
  addTag(tag) {
    self.tags.push({
      id: tag.id,
      title: tag.title,
      color: tag.color
    });
  },
  toggleTag(tag) {
    var tagExists = self.tags.map(x => x.id).includes(tag.id);
    if(tagExists)
      self.deleteTag(tag.id);
    else {
      self.addTag(tag);
    }
  },
  reset() {
    self.firstName = "";
    self.lastName = "";
    self.id = 0;
    self.address = "";
    self.notes = "";
    self.phoneNumbers = [];
    self.tags = [];
    self.emailAddresses = [];
  }
})).views(self => ({
  hasTag(id) {
    return self.tags.map(x => x.id).includes(id);
  }
}))


