import { types } from "mobx-state-tree";
var convert = require('color-convert');

export default ColorStore = types.model({
    tagIndex: types.integer,
    color: types.string
}).actions(self => ({
    setColor(color) {
        self.color = color;
    },
    setTagIndex(tagIndex) {
        self.tagIndex = tagIndex;
    },
    setColorHsv(color) {
        var hsvColor = Object.values(color);
        var hsl = convert.hsv.hsl(hsvColor);
        
        self.color = "#" + convert.hsl.hex(hsl);
    }
}))