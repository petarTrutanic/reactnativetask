using System.Collections.Generic;
using System.Linq;
using PhonebookServer.Data.Entities;

namespace PhonebookServer.Models
{
    public class ContactModel
    {
        public int Id {get; set;}
        public string FirstName {get; set;}
        public string LastName {get; set;}
        public string Address {get; set;}
        public string Notes {get; set;}

        public IEnumerable<PhoneNumberModel> PhoneNumbers { get; set; }
        public IEnumerable<EmailAddressModel> EmailAddresses { get; set; }
        public IEnumerable<TagModel> Tags {get; set;}

        public static ContactModel MapTo(Contact contact){
            return new ContactModel{
                Id = contact.Id,
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Address = contact.Address,
                Notes = contact.Notes,
                PhoneNumbers = contact.PhoneNumbers.Where(x => !x.IsDeleted).Select(PhoneNumberModel.MapTo).ToList(),
                EmailAddresses = contact.EmailAddresses.Where(x => !x.IsDeleted).Select(EmailAddressModel.MapTo).ToList()
            };
        }

        public static Contact MapFrom(ContactModel contact){
            return new Contact{
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Address = contact.Address,
                Notes = contact.Notes
            };
        }
    }
}