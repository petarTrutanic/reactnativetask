using PhonebookServer.Data.Entities;

namespace PhonebookServer.Models
{
    public class TagModel
    {
        public int Id {get; set;}
        public string Title {get; set;}
        public string Color {get; set;}

        public static TagModel MapTo(Tag tag){
            return new TagModel{
                Id = tag.Id,
                Title = tag.Title,
                Color = tag.Color
            };
        }

        public static Tag MapFrom(TagModel tag){
            return new Tag{
                Title = tag.Title,
                Color = tag.Color
            };
        }
    }
}