using PhonebookServer.Data.Entities;

namespace PhonebookServer.Models
{
    public class PhoneNumberModel
    {
        public int Id {get; set;}
        public string Number {get; set;}

        public int ContactId {get; set;}

        public static PhoneNumberModel MapTo(PhoneNumber phoneNumber){
            return new PhoneNumberModel{
                Id = phoneNumber.Id,
                Number = phoneNumber.Number,
                ContactId = phoneNumber.ContactId
            };
        }

        public static PhoneNumber MapFrom(PhoneNumberModel phoneNumber){
            return new PhoneNumber{
                Number = phoneNumber.Number,
                ContactId = phoneNumber.ContactId
            };
        }

        public static PhoneNumber MapForCreation(PhoneNumberModel phoneNumber){
            return new PhoneNumber{
                Number = phoneNumber.Number
            };
        }
    }
}