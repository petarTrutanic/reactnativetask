using PhonebookServer.Data.Entities;

namespace PhonebookServer.Models
{
    public class EmailAddressModel
    {
        public int Id {get; set;}
        public string Email {get; set;}

        public int ContactId {get; set;}

        public static EmailAddressModel MapTo(EmailAddress emailAddress){
            return new EmailAddressModel{
                Id = emailAddress.Id,
                Email = emailAddress.Email,
                ContactId = emailAddress.ContactId
            };
        }

        public static EmailAddress MapFrom(EmailAddressModel emailAddress){
            return new EmailAddress{
                Email = emailAddress.Email,
                ContactId = emailAddress.ContactId
            };
        }

        public static EmailAddress MapForCreation(EmailAddressModel emailAddress){
            return new EmailAddress{
                Email = emailAddress.Email
            };
        }
    }
}