using PhonebookServer.Data.Entities;

namespace PhonebookServer.Models
{
    public class TagColorModel
    {
        public int Id {get; set;}
        public string Color {get; set;}
    }
}