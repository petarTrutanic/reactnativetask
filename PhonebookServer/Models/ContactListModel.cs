using System.Collections.Generic;
using PhonebookServer.Data.Entities;

namespace PhonebookServer.Models
{
    public class ContactListModel
    {
        public int Id {get; set;}
        public string FirstName {get; set;}
        public string LastName {get; set;}
        public IEnumerable<TagModel> Tags {get; set;}

        public static ContactListModel MapTo(Contact contact){
            return new ContactListModel{
                Id = contact.Id,
                FirstName = contact.FirstName,
                LastName = contact.LastName
            };
        }
    }
}