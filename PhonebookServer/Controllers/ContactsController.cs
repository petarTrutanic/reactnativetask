﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PhonebookServer.Data.Entities;
using PhonebookServer.Models;
using PhonebookServer.Services;

namespace PhonebookServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        private readonly ContactService _contactService;
        private readonly TagService _tagService;

        public ContactsController(ContactService contactService, TagService tagService)
        {
            _contactService = contactService;
            _tagService = tagService;
        }

        [HttpGet]
        public ActionResult Get(string filter, int tagId)
        {

            var contacts = _contactService
                .GetAll()
                .OrderBy(x => x.FirstName)
                .ThenBy(x => x.LastName)
                .AsEnumerable();

            if(!string.IsNullOrEmpty(filter)){
                contacts = contacts
                .Where(x =>
                    x.FirstName.Contains(filter) ||
                    x.LastName.Contains(filter));
            }

            if(tagId > 0){
                contacts = contacts
                .Where(x =>
                    x.Tags.Select(y => y.Id).Contains(tagId)
                );
            }

            return Ok( new {
                contacts,
                tags = _tagService.GetAllTags().Select(TagModel.MapTo)
            });
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Ok(new {
                Contact = _contactService.Get(id),
                Tags =_tagService.GetAllTags().Select(TagModel.MapTo)
            });
        }

        [HttpPost]
        public ActionResult Post([FromBody] ContactModel model)
        {
            if(model.Id == 0){
                var contact = ContactModel.MapFrom(model);
                contact.PhoneNumbers = model.PhoneNumbers.Select(PhoneNumberModel.MapForCreation).ToList();
                contact.EmailAddresses = model.EmailAddresses.Select(EmailAddressModel.MapForCreation).ToList();
                var contactId = _contactService.Create(contact);

                _tagService.CreateForContact(contactId, model.Tags.Select(x => x.Id));

                return Ok();
            }else{
                _contactService.Update(model);
                return Ok();
            }  
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _contactService.Delete(id);
        }
    }
}
