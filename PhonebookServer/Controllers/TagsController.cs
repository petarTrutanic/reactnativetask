using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PhonebookServer.Models;
using PhonebookServer.Services;

namespace PhonebookServer.Controllers
{
    [Route("api/[controller]")]
    public class TagsController : ControllerBase
    {
        private readonly TagService _tagService;
        public TagsController(TagService tagService)
        {
            _tagService = tagService;
        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(_tagService.GetAllTags().Select(TagModel.MapTo));
        }

        [HttpPost]
        public ActionResult Post([FromBody] IEnumerable<TagModel> tags)
        {
            _tagService.Update(tags);
            return Ok();
        }
    }
}