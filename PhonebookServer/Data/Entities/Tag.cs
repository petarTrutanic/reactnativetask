namespace PhonebookServer.Data.Entities
{
    public class Tag : BaseEntity
    {
        public string Title {get; set;}
        public string Color {get; set;}
    }
}