namespace PhonebookServer.Data.Entities
{
    public class EmailAddress : BaseEntity
    {
        public string Email {get; set;}

        public int ContactId {get; set;}
    }
}