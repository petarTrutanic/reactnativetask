namespace PhonebookServer.Data.Entities
{
    public class PhoneNumber : BaseEntity
    {
        public string Number {get; set;}

        public int ContactId {get; set;}
    }
}