using System.Collections.Generic;

namespace PhonebookServer.Data.Entities
{
    public class Contact : BaseEntity
    {
        public Contact()
        {
            PhoneNumbers = new HashSet<PhoneNumber>();
            EmailAddresses = new HashSet<EmailAddress>();
        }

        public string FirstName {get; set;}
        public string LastName {get; set;}
        public string Address {get; set;}
        public string Notes {get; set;}

        public virtual ICollection<PhoneNumber> PhoneNumbers { get; set; }
        public virtual ICollection<EmailAddress> EmailAddresses { get; set; }
    }
}