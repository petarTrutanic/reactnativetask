using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using PhonebookServer.Data.Entities;

namespace PhonebookServer.Data
{
    public class PhonebookDbContext : DbContext
    {
        public PhonebookDbContext(DbContextOptions<PhonebookDbContext> options) : base(options)         
        {
        }

        public DbSet<PhoneNumber> PhoneNumbers { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<EmailAddress> EmailAddresses { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<ContactTag> ContactTags { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)    
        {                                                 
            modelBuilder.Entity<ContactTag>()             
                .HasKey(x => new {x.ContactId, x.TagId});

            modelBuilder.Entity<Tag>().HasData(
                new Tag
                {
                    Id = 1,
                    Title = "Prijatelj",
                    Color = "#30c23d"
                },
                new Tag
                {
                    Id = 2,
                    Title = "Kolega",
                    Color = "#ffcc00"
                },
                new Tag
                {
                    Id = 3,
                    Title = "Član obitelji",
                    Color = "#99ccff"
                }
            );

            modelBuilder.Entity<Contact>().HasData(
                new Contact
                {
                    Id = 1,
                    FirstName = "Ivan",
                    LastName = "Ivić",
                    Address = "Kod sebe doma",
                    Notes = "Voli popit"
                },
                new Contact
                {
                    Id = 2,
                    FirstName = "Petar",
                    LastName = "Trutanić",
                    Address = "Tome Didolića 18",
                    Notes = "Programer"
                },
                new Contact
                {
                    Id = 3,
                    FirstName = "Tomo",
                    LastName = "Tomić",
                    Address = "Negdi u Splitu",
                    Notes = "Programer"
                }
            );

            modelBuilder.Entity<PhoneNumber>().HasData(
                new PhoneNumber {
                    Id = 1,
                    ContactId = 1,
                    Number = "+385954567894",
                },
                new PhoneNumber {
                    Id = 2,
                    ContactId = 1,
                    Number = "+385954131313",
                },
                new PhoneNumber {
                    Id = 3,
                    ContactId = 2,
                    Number = "+385955015054",
                },
                new PhoneNumber {
                    Id = 4,
                    ContactId = 2,
                    Number = "+385955148904",
                },
                new PhoneNumber {
                    Id = 5,
                    ContactId = 3,
                    Number = "+385955468414",
                },
                new PhoneNumber {
                    Id = 6,
                    ContactId = 3,
                    Number = "+385955754184",
                }
            );

            modelBuilder.Entity<EmailAddress>().HasData(
                new EmailAddress {
                    Id = 1,
                    ContactId = 1,
                    Email = "ivan.trutanic@gmail.com"
                },
                new EmailAddress {
                    Id = 2,
                    ContactId = 1,
                    Email = "ivan.trutanic@hotmail.com"
                },                        
                new EmailAddress {
                    Id = 3,
                    ContactId = 2,
                    Email = "petar.trutanic@gmail.com"
                },
                new EmailAddress {
                    Id = 4,
                    ContactId = 2,
                    Email = "petar.trutanic@hotmail.com"
                },
                new EmailAddress {
                    Id = 5,
                    ContactId = 2,
                    Email = "petar.trutanic@pseudocode.agencys"
                },
                new EmailAddress {
                    Id = 6,
                    ContactId = 3,
                    Email = "tomo@gmail.com"
                }
            );

            modelBuilder.Entity<ContactTag>().HasData(
                new ContactTag
                {
                    ContactId = 1,
                    TagId = 3
                },
                new ContactTag
                {
                    ContactId = 3,
                    TagId = 1
                },
                new ContactTag
                {
                    ContactId = 3,
                    TagId = 2
                }
            );
        }
    }
}