﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PhonebookServer.Migrations
{
    public partial class Added_Seed_Data : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Contacts",
                columns: new[] { "Id", "Address", "FirstName", "IsDeleted", "LastName", "Notes" },
                values: new object[,]
                {
                    { 1, "Kod sebe doma", "Ivan", false, "Ivić", "Voli popit" },
                    { 2, "Tome Didolića 18", "Petar", false, "Trutanić", "Programer" },
                    { 3, "Negdi u Splitu", "Tomo", false, "Tomić", "Programer" }
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "Color", "IsDeleted", "Title" },
                values: new object[,]
                {
                    { 1, "#30c23d", false, "Prijatelj" },
                    { 2, "#ffcc00", false, "Kolega" },
                    { 3, "#99ccff", false, "Član obitelji" }
                });

            migrationBuilder.InsertData(
                table: "ContactTags",
                columns: new[] { "ContactId", "TagId" },
                values: new object[,]
                {
                    { 3, 1 },
                    { 3, 2 },
                    { 1, 3 }
                });

            migrationBuilder.InsertData(
                table: "EmailAddresses",
                columns: new[] { "Id", "ContactId", "Email", "IsDeleted" },
                values: new object[,]
                {
                    { 1, 1, "ivan.trutanic@gmail.com", false },
                    { 2, 1, "ivan.trutanic@hotmail.com", false },
                    { 3, 2, "petar.trutanic@gmail.com", false },
                    { 4, 2, "petar.trutanic@hotmail.com", false },
                    { 5, 2, "petar.trutanic@pseudocode.agencys", false },
                    { 6, 3, "tomo@gmail.com", false }
                });

            migrationBuilder.InsertData(
                table: "PhoneNumbers",
                columns: new[] { "Id", "ContactId", "IsDeleted", "Number" },
                values: new object[,]
                {
                    { 1, 1, false, "+385954567894" },
                    { 2, 1, false, "+385954131313" },
                    { 3, 2, false, "+385955015054" },
                    { 4, 2, false, "+385955148904" },
                    { 5, 3, false, "+385955468414" },
                    { 6, 3, false, "+385955754184" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ContactTags",
                keyColumns: new[] { "ContactId", "TagId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "ContactTags",
                keyColumns: new[] { "ContactId", "TagId" },
                keyValues: new object[] { 3, 1 });

            migrationBuilder.DeleteData(
                table: "ContactTags",
                keyColumns: new[] { "ContactId", "TagId" },
                keyValues: new object[] { 3, 2 });

            migrationBuilder.DeleteData(
                table: "EmailAddresses",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "EmailAddresses",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "EmailAddresses",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "EmailAddresses",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "EmailAddresses",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "EmailAddresses",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PhoneNumbers",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tags",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
