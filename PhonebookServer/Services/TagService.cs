using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PhonebookServer.Data;
using PhonebookServer.Data.Entities;
using PhonebookServer.Models;

namespace PhonebookServer.Services
{
    public class TagService : BaseService<Tag>
    {
        public TagService(PhonebookDbContext serverDb) : base(serverDb)
        {
        }

        public IEnumerable<Tag> GetForContact(int contactId){
            return ServerDb.ContactTags
            .Include(x => x.Tag)
            .Where(x => x.ContactId == contactId && !x.Tag.IsDeleted)
            .Select(x => x.Tag);
        }

        public IEnumerable<Tag> GetAllTags(){
            return Query();
        }

        public void CreateForContact(int contactId, IEnumerable<int> tagIds) {
            ServerDb.ContactTags.AddRange(tagIds.Select(x => new ContactTag{
                ContactId = contactId,
                TagId = x
            }));
            Save();
        }

        public void Update(IEnumerable<TagModel> tagModels){
            var existingTags = Query().ToList();

            var toCreate = tagModels.Where(x => x.Id == 0);
            var toDelete = existingTags.Where(x => !tagModels.Select(y => y.Id).Contains(x.Id));
            var toUpdate = existingTags.Where(x => tagModels.Select(y => y.Id).Contains(x.Id));

            CreateTags(toCreate);
            DeleteTags(toDelete);
            UpdateTags(toUpdate, tagModels);
            Save();
        }

        private void CreateTags(IEnumerable<TagModel> toCreate) {
            CreateManyWithoutSaving(toCreate.Select(TagModel.MapFrom));
        }

        private void DeleteTags(IEnumerable<Tag> tags) {
            foreach(var tag in tags){
                tag.IsDeleted = true;
            }
        }

        private void UpdateTags(IEnumerable<Tag> tags, IEnumerable<TagModel> tagModels) {
            foreach(var tag in tags){
                var tagModel = tagModels.Single(x => x.Id == tag.Id);
                tag.Title = tagModel.Title;
                tag.Color = tagModel.Color;
            }
        }
    }
}