using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PhonebookServer.Data;
using PhonebookServer.Data.Entities;
using PhonebookServer.Models;

namespace PhonebookServer.Services
{
    public class PhoneNumberService : BaseService<PhoneNumber>
    {
        public PhoneNumberService(PhonebookDbContext serverDb) : base(serverDb)
        {
        }

        public void Update(IEnumerable<PhoneNumber> phoneNumbers, IEnumerable<PhoneNumberModel> phoneNumbersModel){
            var phoneNumbersList = phoneNumbers.Where(x => !x.IsDeleted).ToList();

            var toCreate = phoneNumbersModel.Where(x => x.Id == 0);
            var toDelete = phoneNumbersList.Where(x => !phoneNumbersModel.Select(y => y.Id).Contains(x.Id));
            var toUpdate = phoneNumbersList.Where(x => phoneNumbersModel.Select(y => y.Id).Contains(x.Id));

            CreatePhoneNumbers(toCreate);
            DeletePhoneNumbers(toDelete);
            UpdatePhoneNumbers(toUpdate, phoneNumbersModel);
            Save();
        }

        private void CreatePhoneNumbers(IEnumerable<PhoneNumberModel> toCreate) {
            CreateManyWithoutSaving(toCreate.Select(PhoneNumberModel.MapFrom));
        }

        private void DeletePhoneNumbers(IEnumerable<PhoneNumber> phoneNumbers) {
            foreach(var phoneNumber in phoneNumbers){
                phoneNumber.IsDeleted = true;
            }
        }

        private void UpdatePhoneNumbers(IEnumerable<PhoneNumber> phoneNumbers, IEnumerable<PhoneNumberModel> phoneNumbersModel) {
            foreach(var phoneNumber in phoneNumbers){
                var phoneNumberModel = phoneNumbersModel.Single(x => x.Id == phoneNumber.Id);
                phoneNumber.Number = phoneNumberModel.Number;
            }
        }
    }
}