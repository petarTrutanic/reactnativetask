using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using PhonebookServer.Data;
using PhonebookServer.Data.Entities;

namespace PhonebookServer.Services
{
    public class BaseService<T> where T : BaseEntity
    {
        protected readonly PhonebookDbContext ServerDb;
        protected readonly DbSet<T> DbSet;

        public BaseService(PhonebookDbContext serverDb)
        {
            ServerDb = serverDb;
            DbSet = ServerDb.Set<T>();
        }

        public virtual IQueryable<T> Query()
        {
            return DbSet.Where(x => !x.IsDeleted);
        }

        public virtual void Delete(params T[] entities)
        {
            foreach (var entity in entities)
            {
                entity.IsDeleted = true;
            }
            ServerDb.SaveChanges();
        }

        public virtual void Delete(int id)
        {
            var entity = DbSet.Find(id);
            if (entity != null) entity.IsDeleted = true;

            ServerDb.SaveChanges();
        }

        public virtual T Single(int id)
        {
            return DbSet.Find(id);
        }

        public virtual T SingleIncluding(int id, params Expression<Func<T, object>>[] includeProperties)
        {
            var query = Query();
            foreach (var property in includeProperties)
                query = query.Include(property);
            return query.Single(x => x.Id == id);
        }

        public virtual int Create(T entity, bool shouldSave = true)
        {
            DbSet.Add(entity);
            if (shouldSave)
                ServerDb.SaveChanges();
            return entity.Id;
        }

        public void Save() => ServerDb.SaveChanges();

        public virtual void CreateManyWithoutSaving(IEnumerable<T> entities)
        {
            DbSet.AddRange(entities);
        }

        public virtual void DeleteManyWithoutSaving(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.IsDeleted = true;
            }
        }

        public virtual bool Exists(int id) =>
            DbSet.Any(x => !x.IsDeleted && x.Id == id);
    }
}