using System.Collections.Generic;
using System.Linq;
using PhonebookServer.Data;
using PhonebookServer.Data.Entities;
using PhonebookServer.Models;

namespace PhonebookServer.Services
{
    public class ContactTagService
    {
        private readonly TagService _tagService;
        private readonly PhonebookDbContext _serverDb;

        public ContactTagService(PhonebookDbContext serverDb, TagService tagService)
        {
            _serverDb = serverDb;
            _tagService = tagService;
        }

        public void UpdateTagsForContact(int contactId, IEnumerable<TagModel> model){
            var existingTags = _tagService.GetForContact(contactId).ToList();

            var tagsToDelete = existingTags.Where(x => !model.Select(y => y.Id).Contains(x.Id));
            var tagsToCreate = model.Where(x => !existingTags.Select(y => y.Id).Contains(x.Id));

            Create(tagsToCreate, contactId);
        	Delete(tagsToDelete, contactId);
        }

        private void Create(IEnumerable<TagModel> tagsToCreate, int contactId){
            _serverDb.ContactTags.AddRange(tagsToCreate.Select(x => new ContactTag{
                ContactId = contactId,
                TagId = x.Id
            }));
            _serverDb.SaveChanges();
        }

        private void Delete(IEnumerable<Tag> tagsToDelete, int contactId){
            var contactTagsToDelete = _serverDb.ContactTags.Where(x => tagsToDelete.Select(y => y.Id).Contains(x.TagId) && x.ContactId == contactId);
            _serverDb.ContactTags.RemoveRange(contactTagsToDelete);
            _serverDb.SaveChanges();
        }
    }
}