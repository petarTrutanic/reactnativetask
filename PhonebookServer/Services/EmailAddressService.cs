using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PhonebookServer.Data;
using PhonebookServer.Data.Entities;
using PhonebookServer.Models;

namespace PhonebookServer.Services
{
    public class EmailAddressService : BaseService<EmailAddress>
    {
        public EmailAddressService(PhonebookDbContext serverDb) : base(serverDb)
        {
        }

        public void Update(IEnumerable<EmailAddress> emailAddresses, IEnumerable<EmailAddressModel> emailAddressesModel){
            var emailAddressList = emailAddresses.Where(x => !x.IsDeleted).ToList();

            var toCreate = emailAddressesModel.Where(x => x.Id == 0);
            var toDelete = emailAddressList.Where(x => !emailAddressesModel.Select(y => y.Id).Contains(x.Id));
            var toUpdate = emailAddressList.Where(x => emailAddressesModel.Select(y => y.Id).Contains(x.Id));

            CreateEmailAddresses(toCreate);
            DeleteEmailAddresses(toDelete);
            UpdateEmailAddresses(toUpdate, emailAddressesModel);
            Save();
        }

        private void CreateEmailAddresses(IEnumerable<EmailAddressModel> toCreate) {
            CreateManyWithoutSaving(toCreate.Select(EmailAddressModel.MapFrom));
        }

        private void DeleteEmailAddresses(IEnumerable<EmailAddress> emailAddresses) {
            foreach(var emailAddress in emailAddresses){
                emailAddress.IsDeleted = true;
            }
        }

        private void UpdateEmailAddresses(IEnumerable<EmailAddress> emailAddresses, IEnumerable<EmailAddressModel> emailAddressesModel) {
            foreach(var emailAddress in emailAddresses){
                var emailAddressModel = emailAddressesModel.Single(x => x.Id == emailAddress.Id);
                emailAddress.Email = emailAddressModel.Email;
            }
        }
    }
}