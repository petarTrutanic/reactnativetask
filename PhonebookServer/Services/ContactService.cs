using System.Collections.Generic;
using System.Linq;
using PhonebookServer.Data;
using PhonebookServer.Data.Entities;
using PhonebookServer.Models;

namespace PhonebookServer.Services
{
    public class ContactService : BaseService<Contact>
    {
        private readonly TagService _tagService;
        private readonly PhoneNumberService _phoneNumberService;
        private readonly EmailAddressService _emailAddressService;
        private readonly ContactTagService _contactTagService;

        public ContactService(
            PhonebookDbContext serverDb,
            TagService tagService,
            PhoneNumberService phoneNumberService,
            EmailAddressService emailAddressService,
            ContactTagService contactTagService)
            : base(serverDb)
        {
            _tagService = tagService;
            _phoneNumberService = phoneNumberService;
            _emailAddressService = emailAddressService;
            _contactTagService = contactTagService;
        }

        public IEnumerable<ContactListModel> GetAll(){
            var contacts = Query()
            .Select(ContactListModel.MapTo)
            .ToList();
                            
            foreach(var contact in contacts){
                var tags = _tagService.GetForContact(contact.Id);
                contact.Tags = tags.Select(TagModel.MapTo).ToList();
            }

            return contacts;
        }

        public ContactModel Get(int id){
            if(!Exists(id))
                return null;

            var contact = SingleIncluding(id, x => x.PhoneNumbers, x => x.EmailAddresses);
            var tags = _tagService.GetForContact(id);

            var model = ContactModel.MapTo(contact);
            model.Tags = tags.Select(TagModel.MapTo);

            return model;
        }

        public void Update(ContactModel model){
            if(!Exists(model.Id))
                return;

            var entity = SingleIncluding(model.Id, x => x.PhoneNumbers, x => x.EmailAddresses);

            entity.FirstName = model.FirstName;
            entity.LastName = model.LastName;
            entity.Address = model.Address;
            entity.Notes = model.Notes;
            Save();

            _phoneNumberService.Update(entity.PhoneNumbers, model.PhoneNumbers);
            _emailAddressService.Update(entity.EmailAddresses, model.EmailAddresses);
            _contactTagService.UpdateTagsForContact(model.Id, model.Tags);
        }
    }
}